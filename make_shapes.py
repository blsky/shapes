#!/usr/bin/env python
#coding=utf-8
###############################################
# Author: blsky
# mail: blsky339 AT gmail
###############################################

import argparse
import cv2
from   math import cos, sin, radians, fabs
import numpy as np
import os
from   PIL import Image
import random
from   random import randint
import sys
import webcolors

def err(parser,msg):
  print('============================================')
  print(msg)
  print('============================================')
  parser.print_help()
  sys.exit()

def getCmap():
  # shall write png 8 bit. don't know how to do it with cv2. using PIL...

  #https://pillow.readthedocs.io/en/4.1.x/reference/Image.html
  # Image.putpalette(data, rawmode='RGB')
  #  The image must be a "P" or "L"  image, and the palette sequence must contain 768 integer values,
  # where each group of three values represent the red, green, and blue values for the corresponding pixel index.
  #  From PascalVOC VOCdevkit/VOCcode/VOClabelcolormap.m or https://qiita.com/mine820/items/725fe55c095f28bffe87
  #  PNG 8 bit code: 0 is black. Classes (mark) from 1 to 254. 255 is void

  # adapted from VOCdevkit/VOCcode/VOClabelcolormap.m

  cmap=[]
  
  for id in range(256):
    r=0
    g=0
    b=0
    for jc in range(8):
      bit0 =  id & 1
      bit1 = (id & 2) >> 1
      bit2 = (id & 4) >> 2
      r = r | (bit0 << (7-jc))
      g = g | (bit1 << (7-jc))
      b = b | (bit2 << (7-jc))
      id = id >> 3
    cmap.append(r)
    cmap.append(g)
    cmap.append(b)
  return cmap

def make_star():
  #               1
  # 1             /\
  # 2          10/  \2
  # 3     9 \----    ----/3
  # 4        \    0      /
  # 5       8 \        \ 4
  # 6        /   /6 \   \
  # 7       /. /      \  \
  # 8       7           5
  # 9
  # 10
  # 0  (center)
  #  Inner part: 1,3,5,7,9
  #  Outer part: 0,2,4,6,8

  r72 = radians(72)  # 72=360/5 degrees
  r36 = radians(36)  # 36=72/2 degrees

  d_inner=0.5
  d_outer=d_inner*2*cos(r36) # short. Long version ... cos(36)+sin(36)/tg(72)

  pts=np.zeros((10,3),np.float)

  for i in range(5):
    pts[i*2  ]=[d_outer*sin(    i*r72),d_outer*cos(    i*r72),1.0] # OUTER
    pts[i*2+1]=[d_inner*sin(r36+i*r72),d_inner*cos(r36+i*r72),1.0] # INNER 

  return pts

def put_shape(img,voc_class,voc_obj,t,a, class_num, obj_num):
  size    =randint(int(a.min_scale*max_size),int(a.max_scale*max_size))
  offset  =int(size*1.07)
  x = int(randint(-(a.w-offset),a.w-offset)*a.max_offset*.5 + a.w/2)
  y = int(randint(-(a.h-offset),a.h-offset)*a.max_offset*.5 + a.h/2)
  
  shape_color,shape_g=select_color(a,t,colors)

  if a.ann[0]=='v': # voc
    voc_t=int(a.ann[2]) # thickness

  yolo_ann=0

  shape=np.zeros((img.shape[0],img.shape[1]),np.uint8)   
  
  if t=='c': # circle
    rd=int(size/2)
    cv2.circle(shape , (x,y),rd,1,a.t)
    if a.ann[0:2]=='vf': # filled voc
      cv2.circle(voc_class, (x,y),rd,class_num,-1)
      cv2.circle(voc_class, (x,y),rd,255      ,voc_t)
      cv2.circle(voc_obj  , (x,y),rd,obj_num  ,-1)
      cv2.circle(voc_obj  , (x,y),rd,255      ,voc_t)
    elif a.ann[0:2]=='vl': #  voc line
        cv2.circle(voc_class, (x,y),rd,class_num,voc_t)
        cv2.circle(voc_obj  , (x,y),rd,obj_num  ,voc_t)
    elif a.ann=='yolo':
      yolo_ann=str(class_num)+' '+str(x*1.0/a.w)+' '+str(y*1.0/a.h)+' '+str(size*1.0/a.w)+' '+str(size*1.0/a.h)+'\n'
  else:
    if   t=='t':
      pts=p_triangle
    elif t=='q':
      pts=p_square
    else:
      pts=p_star
      
    ang =randint(-a.max_rot,a.max_rot)
    center = tuple(np.array([0,0]))
    rot_mat = cv2.getRotationMatrix2D(center, ang, 1.0)
    rot_pts = rot_mat.dot(pts.T).T
    rot_pts=rot_pts*size/2
    rot_pts=rot_pts+np.array([x,y])+.5 # .5 if for rounding
    rot_pts=np.array(rot_pts,np.int32)

    if a.t<0:
      cv2.fillPoly(shape, [rot_pts], 1)
    else:
      cv2.polylines(shape, [rot_pts], True, 1, thickness=a.t)

    if a.ann[0:2]=='vf': # filled voc
      cv2.fillPoly( voc_class, [rot_pts],       class_num)
      cv2.polylines(voc_class, [rot_pts], True, 255, voc_t)
      cv2.fillPoly( voc_obj  , [rot_pts],       obj_num)
      cv2.polylines(voc_obj  , [rot_pts], True, 255, voc_t)
    elif a.ann[0:2]=='vl': # voc line
      cv2.polylines(voc_class, [rot_pts], True, class_num, voc_t)
      cv2.polylines(voc_obj  , [rot_pts], True, obj_num  , voc_t)
    elif a.ann=='yolo':
      min_x=np.min(rot_pts[:,0])
      max_x=np.max(rot_pts[:,0])
      min_y=np.min(rot_pts[:,1])
      max_y=np.max(rot_pts[:,1])
      yolo_ann=str(class_num)+' '+str((min_x+max_x)*.5/a.w)+' '+str((min_y+max_y)*.5/a.h)+' '+str((max_x-min_x)*1.0/a.w)+' '+str((max_y-min_y)*1.0/a.h)+'\n'

  put_color(a,img,shape,shape_color, shape_g)     
      
  return yolo_ann

def get_patterns():
  patt_dir='patt/'
  pngs=[f for f in os.listdir(patt_dir) if f.endswith('.png')]
  return [cv2.imread(patt_dir+png) for png in pngs]

def put_color(a,img,shape,color,g):
  # shape has '1' on shape position, '0' otherwise

  shape_pos=np.array(np.where(shape==1))
  pos_x_min=np.min(shape_pos[1])
  pos_x_max=np.max(shape_pos[1])
  pos_y_min=np.min(shape_pos[0])
  pos_y_max=np.max(shape_pos[0])

  shape_fit=shape[pos_y_min:pos_y_max+1,pos_x_min:pos_x_max+1]
  
  shape3=cv2.merge((shape,shape,shape))
  shape_fit3=cv2.merge((shape_fit,shape_fit,shape_fit))
  if g==0.0: # no gradient
    grad=shape_fit3*1.0
  else:  
    pattern=patterns[randint(0,len(patterns)-1)]

    shape_w=pos_x_max-pos_x_min+1
    shape_h=pos_y_max-pos_y_min+1

    # step 1: resize pattern to up N times w/h
    resize_idx=3
    resized=cv2.resize(pattern,(shape_w*resize_idx,shape_h*resize_idx))

    # step 2: crop on proper selected place
    x_pos=randint(0,shape_w*(resize_idx-1)-1)
    y_pos=randint(0,shape_h*(resize_idx-1)-1)
    cropped=resized[y_pos:y_pos+shape_h,x_pos:x_pos+shape_w]
   
    # grad (deal as 3 channels)
    cropped_shape=cropped*shape_fit3

    max_c=np.max(cropped_shape)
    min_c=np.min(cropped_shape)
    diff_c=max_c-min_c

    min_value=1.0-g
    grad=(cropped_shape-min_c)*1.0/diff_c*g + min_value

  shape_color=np.zeros((shape.shape[0],shape.shape[1],3))
  shape_color[pos_y_min:pos_y_max+1,pos_x_min:pos_x_max+1]= grad*shape_fit3 
  
  if a.channels==1:
    shape_color=shape_color[:,:,0]*color
    img[shape==1]=shape_color[shape==1]
  else:
    shape_color[:,:,0]=shape_color[:,:,0]*color[2]
    shape_color[:,:,1]=shape_color[:,:,1]*color[1]
    shape_color[:,:,2]=shape_color[:,:,2]*color[0]
    img[shape==1]=shape_color[shape==1]
        
def select_color(a,t,colors):
  colors1=colors[t+'1']
  colors2=colors[t+'2']
  gs=colors[t+'g']
  
  # if many color range defined, select one.
  idx=randint(0,len(colors1)-1)

  color1=colors1[idx]
  color2=colors2[idx]
  g=gs[idx]
  
  if a.channels==1:
    color=randint(min(color1,color2),max(color1,color2))
  else:
    r_=randint(min(color1[0],color2[0]),max(color1[0],color2[0]))
    g_=randint(min(color1[1],color2[1]),max(color1[1],color2[1]))
    b_=randint(min(color1[2],color2[2]),max(color1[2],color2[2]))
    color=(r_,g_,b_)
    
  return color,g
    
def get_colors(a,colors):
  def_color1=[]
  def_color2=[]
  def_colorg=[]

  color_num=0
  for i in range(len(colors)):
    def_color1.append(0)
    def_color2.append(0)
    def_colorg.append(0.0)
    cnt=0
    for part in colors[i].split(','):
      if part[len(part)-1]=='%':
        def_colorg[color_num]=int(part[0:len(part)-1])/100.0
        if cnt==1:
          def_color2[color_num]=def_color1[color_num]
      else:        
        if (a.channels==3):
          try:
            color=webcolors.name_to_rgb(part)
          except Exception:
            try:
              color=webcolors.hex_to_rgb('#'+part)
            except Exception:
              err(parser,'Wrong color code for 3 channels '+part)
        else:
          try:
            if (len(part)>2):
              err(parser,'Wrong color code (0 to ff) for 1 channel '+part)
            color=int(part,16) # 1 channel
          except Exception:
            err(parser,'Wrong color code (0 to ff) for 1 channel '+part)
             
        if (cnt==0):
          def_color1[color_num]=color
          def_color2[color_num]=color # in case we don't have 2nd color defined.
        else:
          def_color2[color_num]=color
      cnt=cnt+1    
    color_num=color_num+1
  return def_color1,def_color2,def_colorg

if __name__ == "__main__":

  parser = argparse.ArgumentParser(add_help=False, description='generate circle/triangle/square/star png shapes')
  parser.add_argument('-w'      , dest='w'      ,default=-1, type=int, help='width  in pixels')
  parser.add_argument('-h'      , dest='h'      ,default=-1, type=int, help='height in pixels')
  parser.add_argument('-type'   , dest='type'      ,default=None, help='shape type (c: circle, t: triangle, q: square, s: st  ar, 1-9: mixed number of shapes)')
  parser.add_argument('-channels',dest='channels'  ,default=1, choices=[1,3], type=int, help='number of color channels(1 or 3)')
  parser.add_argument('-colors'  , dest='colors'  ,action='append', help='default color for all shapes. Format: <color1>,<color2>,<grad%%>. <color1> or <color2> is a number(8 bits for 1 channel, 24 for 3), or a color name(3 channels) string(webcolors).  <grad%%> is max graduation of colors(This selects one pattern from png files). <color2> and <grad%%> can be omitted. If the last number terminates with %%, this will be <grad%%>. If <color2> is not given, <color1> will be used. If <color2> is given, a color is selected between <color1> and <color2>. If this option appears multiple times, a color is selected among all options. Default: white, no grad')
  parser.add_argument('-cb'     , dest='cb'     ,action='append', help='background color. Format is same as -color option. Default: black')
  parser.add_argument('-cc'     , dest='cc'     ,action='append', help='circle color. Overrides circle color set with -color option.. Default: white')
  parser.add_argument('-ct'     , dest='ct'     ,action='append', help='triangle color. Overrides triangle color set with -color option. Default: white')
  parser.add_argument('-cq'     , dest='cq'     ,action='append', help='square color. Overrides square color set with -color option. Default: white')
  parser.add_argument('-cs'     , dest='cs'     ,action='append', help='star color. Overrides star color set with -color option. Default: white')
  parser.add_argument('-t'      , dest='t'      ,default=-1, type=int, help='line thickness. fill shape if negative')
  parser.add_argument('-max_rot', dest='max_rot',default=0 , type=int, help='max rotation (0 to 180), def=0')
  parser.add_argument('-path'   , dest='path'   ,default=None, help='output path')
  parser.add_argument('-num'    , dest='num'    ,default=1   , type=int, help='number of images to generate, def=1')
  parser.add_argument('-min_scale' , dest='min_scale'    ,default=.5, type=float, help='min scale(0 to 1), def=.5')
  parser.add_argument('-max_scale' , dest='max_scale'    ,default=.5, type=float, help='max scale(0 to 1), def=.5')
  parser.add_argument('-max_offset', dest='max_offset'   ,default=0 , type=float, help='max position offset from center. depend on scale (0 to 1.0). def=0')
  parser.add_argument('-ann', dest='ann'  ,default='no' , type=str, help='annotation type (no: none, vf<0-9>: filled voc (0-9 is thickness of void part. Has effect only for negative thickness ), vl<0-9>: voc line, yolo: darknet/yolo)')
  parser.add_argument('-classes', dest='classes'  ,default='1,2,3,4' , type=str, help='comma separated class numbers for annotation(no spaces). Used for Pascal Voc and yolo. <circle>,<triangle>,<square>,<star> ')

  patterns=get_patterns()

  a = parser.parse_args()

  if a.w<0 or a.h<0:
    err(parser,'Shall set w and h to positive numbers')
  if a.max_rot<0 or a.max_rot>180:
    err(parser,'Wrong max_rot')
  if a.path==None:
    err(parser,'Shall set path')

  num_shapes=0
  if a.type!='c' and a.type!='t' and a.type!='q' and a.type!='s':
    if (type(a.type)==str):
      num_shapes=int(a.type)
      if num_shapes<1 or num_shapes>9:
        err(parser,'wrong type')
    else:
      err(parser,'wrong type')

  colors=dict()

  if a.cb!=None:  # background color
    def_color1,def_color2,def_colorg=get_colors(a,a.cb)
  else:
    if a.channels==1:
      def_color1=[0]
      def_color2=[0]
    else: # 3 channels
      def_color1=[(0,0,0)]
      def_color2=[(0,0,0)]
    def_colorg=[0.0]
  
  colors['b1']=def_color1
  colors['b2']=def_color2
  colors['bg']=def_colorg

  def_color1=[255]
  def_color2=[255]
  def_colorg=[0.0]

  if a.colors!=None:
    def_color1,def_color2,def_colorg=get_colors(a,a.colors)
  
  colors['c1']=def_color1
  colors['c2']=def_color2
  colors['cg']=def_colorg
  colors['t1']=def_color1
  colors['t2']=def_color2
  colors['tg']=def_colorg
  colors['q1']=def_color1
  colors['q2']=def_color2
  colors['qg']=def_colorg
  colors['s1']=def_color1
  colors['s2']=def_color2
  colors['sg']=def_colorg

  if a.cc!=None: # circle
    colors['c1'],  colors['c2'],  colors['cg'] = get_colors(a,a.cc)
  if a.ct!=None: # triangle
    colors['t1'],  colors['t2'],  colors['tg'] = get_colors(a,a.ct)
  if a.cq!=None: # square
    colors['q1'],  colors['q2'],  colors['qg'] = get_colors(a,a.qt)
  if a.cs!=None: # star
    colors['s1'],  colors['s2'],  colors['sg'] = get_colors(a,a.st)

  voc_image_dir='JPEGImages'
  voc_class_dir='SegmentationClass'
  voc_obj_dir='SegmentationObject'
  
  if (type(a.ann)==str):
    if (a.ann[0]=='v'):
      if len(a.ann)!=3 :
        err(parser,'wrong annotation thickness for voc')
      if (a.ann[2]<'0' or a.ann[2]>'9'):
        err(parser,'wrong annotation thickness range for voc')
      if not os.path.exists(a.path+'/'+voc_image_dir):
        os.makedirs(a.path+'/'+voc_image_dir)
      if not os.path.exists(a.path+'/'+voc_class_dir):
        os.makedirs(a.path+'/'+voc_class_dir)
      if not os.path.exists(a.path+'/'+voc_obj_dir):
        os.makedirs(a.path+'/'+voc_obj_dir)
    elif a.ann!='yolo' and a.ann!='no':
      err(parser,'wrong annotation name '+ a.ann)
  else:
    err(parser,'wrong annotation type')
  
  classes_=a.classes.split(',')
  classes=dict()
  if (len(classes_)!=4):
    err(parser,'wrong number of classes. Shall be 4(comma separated)(circle, triangle, square, star)')
  else:
    classes['c']=int(classes_[0])
    classes['t']=int(classes_[1])
    classes['q']=int(classes_[2])
    classes['s']=int(classes_[3])
  
  xpos=cos(radians(30))
  p_triangle=np.array([[0.0,1.0,1.0],[-xpos,-.5,1.0],[xpos,-.5,1.0]]) # note: 3rd 1.0 used for rotation
  p_square  =np.array([[-1.0,-1.0,1.0],[1.0,-1.0,1.0],[1.0,1.0,1.0],[-1.0,1.0,1.0]])*cos(radians(45))
  p_star    =make_star()

  max_size = a.w
  if a.h<a.w:
    max_size=a.h

  cmap=getCmap()
  
  for img_num in range(a.num):

    if a.channels==1:
      img=np.zeros((a.h,a.w),np.uint8)
    else: # 3 channels
      img=np.zeros((a.h,a.w,3),np.uint8)

    bg_color,bg_g=select_color(a,'b',colors)

    bg_shape=np.ones((a.h,a.w),np.uint8)
    put_color(a,img,bg_shape,bg_color, bg_g)
  
    voc_obj=0
    voc_class=0
    if (a.ann[0]=='v'): # Pascal VOC 
      # I am mixing PILL Image with cv2.
      # Reason: I started with cv2, but realized I cannot set png 8 bit color with cv2.
      voc_class=np.zeros((a.h,a.w),np.uint8)
      voc_obj  =np.zeros((a.h,a.w),np.uint8)
    
    shape_type=a.type
    if num_shapes==0: # use desired shape
      yolo_ann=put_shape(img,voc_class,voc_obj,a.type,a, classes[a.type], 1)
      if a.ann=='yolo':  
        yolo_name=a.path+'/'+shape_type+'_'+str(img_num).zfill(5)+'.txt'
        yolo_file = open(yolo_name,'w')
        yolo_file.write(yolo_ann)
        yolo_file.close()

    else: # select one shape
      for shape_num in range(num_shapes):
        shape=randint(0,3)
        shape_type='m' # mixed
        if   shape==0: # circle
          yolo_ann=put_shape(img,voc_class,voc_obj,'c',a, classes['c'], shape_num+1)
        elif shape==1: # triangle                                         
          yolo_ann=put_shape(img,voc_class,voc_obj,'t',a, classes['t'], shape_num+1)
        elif shape==2: # square                                           
          yolo_ann=put_shape(img,voc_class,voc_obj,'q',a, classes['q'], shape_num+1)
        else:          # star                                             
          yolo_ann=put_shape(img,voc_class,voc_obj,'s',a, classes['s'], shape_num+1)
        
        if a.ann=='yolo':
          w_mode='a'
          if shape_num==0:
            w_mode='w'
          yolo_name=a.path+'/'+shape_type+'_'+str(img_num).zfill(5)+'.txt'
          yolo_file = open(yolo_name,w_mode)
          yolo_file.write(yolo_ann)
          yolo_file.close()

    voc_path=''
    if a.ann[0]=='v': # voc
      voc_path=voc_image_dir+'/'
   
    image_name=a.path+'/'+voc_path+shape_type+'_'+str(img_num).zfill(5)+'.jpg'
    print('Generating:'+image_name);
    cv2.imwrite(image_name,img)
    
    if a.ann[0]=='v': # voc
      class_name=a.path+'/'+voc_class_dir+'/'+shape_type+'_'+str(img_num).zfill(5)+'.png'
      print('Generating:'+class_name);
      im_class=Image.fromarray(voc_class)
      im_class.putpalette(cmap)
      im_class.save(class_name)
      
      obj_name=a.path+'/'+voc_obj_dir+'/'+shape_type+'_'+str(img_num).zfill(5)+'.png'
      print('Generating:'+obj_name);
      im_obj=Image.fromarray(voc_obj)
      im_obj.putpalette(cmap)
      im_obj.save(obj_name)


      

      
