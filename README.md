## READ FIRST before using

This software has being developed for educational purpose, to be used as training data for deep learning.

However, I could **not** develop a simple classification algorithm that learns with it.

I've tried with many simple network, as well as many configuration of shapes. No way.

The only way the network learned the shapes was when my training data set contained both MNIST and these shapes (Hope I could have time to report the details someday).

I have no idea of the reasons, but will further work to try to understand the behavior of a simple network. Or, if someone knows the reason, please educate me.

Anyway, I am sharing this tool hoping it could be useful to someone.

I appreciate if you give me credits is this helped you.

## Features
* Generate circle, triangle, square and or star
* Single or multiple shapes per image
* 1 or 3 color channels
* Can define color range for each of shape as well as background
* Can further use gradient color patterns
* Can generate full shapes or outline
* Supports rotation
* Can set size scales
* Supports annotation for darknet/yolo and Pascal VOC formats
* Set output path

## Usage

```markdown
usage: make_shapes.py [-w W] [-h H] [-type TYPE] [-channels {1,3}]
                      [-colors COLORS] [-cb CB] [-cc CC] [-ct CT] [-cq CQ]
                      [-cs CS] [-t T] [-max_rot MAX_ROT] [-path PATH]
                      [-num NUM] [-min_scale MIN_SCALE] [-max_scale MAX_SCALE]
                      [-max_offset MAX_OFFSET] [-ann ANN] [-classes CLASSES]

generate circle/triangle/square/star png shapes

optional arguments:
  -w W                  width in pixels
  -h H                  height in pixels
  -type TYPE            shape type (c: circle, t: triangle, q: square, s: st
                        ar, 1-9: mixed number of shapes)
  -channels {1,3}       number of color channels(1 or 3)
  -colors COLORS        default color for all shapes. Format:
                        <color1>,<color2>,<grad%>. <color1> or <color2> is a
                        number(8 bits for 1 channel, 24 for 3), or a color
                        name(3 channels) string(webcolors). <grad%> is max
                        graduation of colors(This selects one pattern from png
                        files). <color2> and <grad%> can be omitted. If the
                        last number terminates with %, this will be <grad%>.
                        If <color2> is not given, <color1> will be used. If
                        <color2> is given, a color is selected between
                        <color1> and <color2>. If this option appears multiple
                        times, a color is selected among all options. Default:
                        white no grad
  -cb CB                background color. Format is same as -color option.
                        Default: black
  -cc CC                circle color. Overrides circle color set with
                        -color option. Default: white
                        Default: white
  -ct CT                triangle color. Overrides triangle color set with
                        -color option. Default: white
  -cq CQ                square color. Overrides square color set with -color
                        option. Default: white
  -cs CS                star color. Overrides star color set with -color
                        option. Default: white
  -t T                  line thickness. fill shape if negative
  -max_rot MAX_ROT      max rotation (0 to 180), def=0
  -path PATH            output path
  -num NUM              number of images to generate, def=1
  -min_scale MIN_SCALE  min scale(0 to 1), def=.5
  -max_scale MAX_SCALE  max scale(0 to 1), def=.5
  -max_offset MAX_OFFSET
                        max position offset from center. depend on scale (0 to
                        1.0). def=0
  -ann ANN              annotation type (no: none, vf<0-9>: filled voc (0-9 is
                        thickness of void part. Has effect only for negative
                        thickness ), vl<0-9>: voc line, yolo: darknet/yolo)
  -classes CLASSES      comma separated class numbers for annotation(no
                        spaces). Used for Pascal Voc and yolo.
                        <circle>,<triangle>,<square>,<star>

```

## Examples

### Example 1: Pascal VOC (filled)


    python make_shapes.py  -w 500 -h 400 -type 5 -channels 3 -colors red,ff00ff,100% -colors yellow,10% -colors 001010,blue,50% -cb black,111111,0% -path  /tmp -num 100 -min_scale=.1 -max_scale=.3 -max_rot=10 -max_offset=.8 -ann vf2 -t -1
    
    Notes:
    -type 5    : Generates 5 shapes per image
    -colors    : There are 3. One will be selected for each shape
    -ann vf2   : Filled pascal VOC annotation with void thickness 2
    -t -1      : Filled shapes
    
Result example:

![](pics/voc_full.png "Left: image Center: Object  Right: CLass")  
 
 
### Example 2: Pascal VOC (outline on image and VOC)


    python make_shapes.py  -w 500 -h 400 -type 5 -channels 3 -colors red,ff00ff,100% -colors yellow,10% -colors 001010,blue,50% -cb black,111111,0% -path  /tmp -num 100 -min_scale=.1 -max_scale=.3 -max_rot=10 -max_offset=.8 -ann vl2 -t 2
    
     Notes:
     -ann vl2  : Pascal VOC of outline
     -t 2      : Shapes with outline thickness 2
     
![](pics/voc_line.png)


### Example 3: Setting different class numbers
The default class numbers are:

    circle:1
    triangle:2
    square:3
    star:4
    
You can change the default as follows (both Pascal VOC and darknet/yolo)
 
 
    python make_shapes.py  -w 500 -h 400 -type 5 -channels 3 -colors red,ff00ff,100% -colors yellow,10% -colors 001010,blue,50% -cb black,111111,0% -path  /tmp -num 100 -min_scale=.1 -max_scale=.3 -max_rot=10 -max_offset=.8 -ann vf2 -t -1 -classes 10,12,15,20

    Note:
    -classes 10,12,15,20
    This makes:
    circle:10
    triangle:12
    square:15
    star:20
    
 Example: (See the color changes on the right picture)   
 
![](pics/classes.png)


### Example 4: Pascal VOC (outline on VOC only)
This is a possible example, although not usefull for AI.

    python make_shapes.py  -w 500 -h 400 -type 5 -channels 3 -colors red,ff00ff,100% -colors yellow,10% -colors 001010,blue,50% -cb black,111111,0% -path  /tmp -num 100 -min_scale=.1 -max_scale=.3 -max_rot=10 -max_offset=.8 -ann vl2 -t -1              


![](pics/voc_full_line.png)


### Example 5: Color gradient
This produces:

* Red circles
* White square and star
* Blue with gradien triangles

    python make_shapes.py  -w 500 -h 400 -type 5 -channels 3 -cc red -ct 000080,0000ff,100% -colors ffffff -cb black,111111,0% -path  /tmp -num 100 -min_scale=.1 -max_scale=.3 -max_rot=10 -max_offset=.8 -ann vf2 -t -1

    Note:
    -cc red   : red circle
    -ct 000080,0000ff,100% : select a color between 00000080 and 0000ff with 100% gradient for triangle
    -color ffffff : white color for other shapes (square and star)
    
    
![](pics/color.jpg)


### Example 6: 1 channel (grayscale), darknet/yolo annotation

    python make_shapes.py  -w 500 -h 400 -type q -channels 1 -colors ff,e0,100% -cb 00,20,0% -path  /tmp -num 10 -min_scale=.3 -max_scale=.7 -max_rot=20 -max_offset=.8 -ann yolo

    Note:
    -type 1  : generates square only

    This also outputs an annotation txt file with the following:
    3 0.606 0.42 0.348 0.435


The darknet/yolo annotation data is for the original full shape. This means that annotation for darknet/yolo works only for single shape on an image. If we have overlapped shapes, the annotation will still contain parts of the hidden shapes.


![](pics/square.jpg)
	
					